Bitapi::Application.routes.draw do
  namespace :v1 do
    resources :resources, only: :index
    resources :feeds, only: :show
  end
end
