class User < ActiveRecord::Base

  has_many :providers
  has_many :feeds

  def provider_names
    @provider_names ||= providers.collect{|p| p.name}
  end

  def has_provider?(name)
    provider_names.include?(name)
  end
end
