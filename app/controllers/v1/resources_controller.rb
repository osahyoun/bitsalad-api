module V1
  class ResourcesController < ActionController::API
    def index
      render json: { hello: :world }, callback: params[:callback]
    end
  end
end
