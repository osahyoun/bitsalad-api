module V1
  class FeedsController < ActionController::API
    def show
      render json: feed.content, callback: params[:callback]
    end

    private

    def feed
      @feed ||= Feed.find(params[:id])
    end
  end
end

