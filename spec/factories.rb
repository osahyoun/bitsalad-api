FactoryGirl.define do 

  factory :user do
    sequence(:email) do |n|
      "person#{n}@example.com"
    end
  end

  factory :provider do
    name "instagram"
    uid "1234"
    token "yyy"
    secret nil
    nickname "joe"
    association :user
  end

  factory :feed do
    association :provider
    association :user
  end

  factory :instagram_feed do
    provider
    user
    _type 'InstagramFeed'
  end
end
