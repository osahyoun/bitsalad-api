require 'rails_helper'

describe V1::FeedsController, type: :controller do
  describe 'GET show' do
    let(:feed) { double('feed', {content: 'foo'}) }

    before do
      allow(Feed).to receive(:find){ feed }
    end

    it 'finds feed by uid' do
      expect(Feed).to receive(:find).with('xyz'){ feed }
      get :show, id: 'xyz'
    end

    it 'renders feed content' do
      get :show, id: 'xyz'
      expect(response.body).to eq('foo')
    end

    it 'wraps body with callback' do
      get :show, id: 'xyz', callback: 'bar'
      expect(response.body).to eq('/**/bar(foo)')
    end
  end
end

